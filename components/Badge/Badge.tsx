import React from 'react';
import classnames from 'classnames';

import styles from './Badge.module.scss';

export interface BadgeProps extends React.HTMLAttributes<HTMLDivElement> {
  active?: boolean;
}

const Badge = React.forwardRef<HTMLDivElement, BadgeProps>(({ children, className, active, ...rest }, ref) => {
  return (
    <div ref={ref} className={classnames(styles.badge, { [styles.active]: active }, className)} {...rest}>
      {children}
    </div>
  );
});

export default React.memo(Badge);
