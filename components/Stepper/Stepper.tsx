import React, { useCallback } from 'react';

import { Step } from '../../types';
import StepperItem from './StepperItem';

import styles from './Stepper.module.scss';

interface StepperProps {
  steps: Step[];
  activeStepIndex?: number;
  onNavigate?: (index: number) => void;
  onFinish?: () => void;
  renderContent?: (step: Step, index: number) => React.ReactNode;
}

const Stepper: React.FC<StepperProps> = ({ steps, activeStepIndex = 0, onNavigate, onFinish, renderContent }) => {
  const handleNext = useCallback(
    (index: number) => {
      if (typeof onNavigate !== 'function') return;

      if (index === steps.length - 1) {
        if (typeof onFinish === 'function') return onFinish();

        return;
      }

      onNavigate(index + 1);
    },
    [steps, onFinish, onNavigate],
  );

  const handleBack = useCallback(
    (index: number) => {
      if (typeof onNavigate !== 'function') return;

      const previousIndex = Math.max(0, index - 1);

      onNavigate(previousIndex);
    },
    [onNavigate],
  );

  const handleToggle = useCallback(
    (index: number, active: boolean) => {
      if (typeof onNavigate !== 'function') return;

      onNavigate(active ? index : -1);
    },
    [onNavigate],
  );

  return (
    <div className={styles.stepper}>
      {steps.map((step, index) => (
        <StepperItem
          key={step.label}
          numberOfSteps={steps.length}
          step={step}
          active={activeStepIndex === index}
          index={index}
          onNext={handleNext}
          onBack={handleBack}
          onToggle={handleToggle}
          renderContent={renderContent}
        />
      ))}
    </div>
  );
};

export default React.memo(Stepper);
