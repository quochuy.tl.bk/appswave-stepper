import React, { useCallback } from 'react';

import { ExpandLess } from '../../icons/ExpandLess';
import { ExpandMore } from '../..//icons/ExpandMore';
import { Badge } from '../Badge';

import styles from './StepperItemHeader.module.scss';

interface StepperItemHeaderProps {
  index: number;
  label: string;
  active?: boolean;
  onToggle?: (active: boolean) => void;
}

const StepperItemHeader: React.FC<StepperItemHeaderProps> = ({ index, label, active, onToggle }) => {
  const handleClick = useCallback(() => {
    if (typeof onToggle !== 'function') return;

    onToggle(!active);
  }, [onToggle, active]);

  return (
    <header className={styles.header} onClick={handleClick}>
      <Badge active={active}>{index + 1}</Badge>
      <h3 className={styles.label}>{label}</h3>
      {active ? <ExpandLess className={styles.expandLess} /> : <ExpandMore className={styles.expandMore} />}
    </header>
  );
};

export default React.memo(StepperItemHeader);
