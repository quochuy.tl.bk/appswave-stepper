import React from 'react';
import AnimateHeight from 'react-animate-height';
import usePrevious from '../../hooks/usePrevious';

import styles from './StepperItemContent.module.scss';

interface StepperItemContentProps {
  active?: boolean;
  first?: boolean;
  last?: boolean;
  onNext?: () => void;
  onBack?: () => void;
  children?: React.ReactNode;
}

const StepperItemContent: React.FC<StepperItemContentProps> = ({ active, first, last, onNext, onBack, children }) => {
  const { value: previousChildren } = usePrevious(children);

  return (
    <AnimateHeight height={active ? 'auto' : 0}>
      <div className={styles.content}>
        {active ? children : previousChildren}
        <div className={styles.actions}>
          {!last && (
            <button className="button is-primary" onClick={onNext}>
              Next
            </button>
          )}
          {!first && (
            <>
              {last ? (
                <button className="button is-primary" onClick={onNext}>
                  Finish
                </button>
              ) : (
                <button className="button is-white" onClick={onBack}>
                  Back
                </button>
              )}
            </>
          )}
        </div>
      </div>
    </AnimateHeight>
  );
};

export default React.memo(StepperItemContent);
