import React, { useCallback, useMemo } from 'react';
import classnames from 'classnames';

import StepperItemContent from './StepperItemContent';
import StepperItemHeader from './StepperItemHeader';
import { Step } from '../../types';

import styles from './StepperItem.module.scss';

interface StepperItemProps {
  index: number;
  step: Step;
  numberOfSteps?: number;
  active?: boolean;
  onNext?: (currentIndex: number) => void;
  onBack?: (currentIndex: number) => void;
  onToggle?: (index: number, active: boolean, step?: Step) => void;
  renderContent?: (step: Step, index: number) => React.ReactNode;
}

const StepperItem: React.FC<StepperItemProps> = ({
  index,
  step,
  numberOfSteps,
  active,
  onNext,
  onBack,
  onToggle,
  renderContent,
}) => {
  const handleNext = useCallback(() => {
    if (typeof onNext !== 'function') return;

    onNext(index);
  }, [index, onNext]);

  const handleBack = useCallback(() => {
    if (typeof onBack !== 'function') return;

    onBack(index);
  }, [index, onBack]);

  const handleToggle = useCallback(
    (newActive: boolean) => {
      if (typeof onToggle !== 'function') return;

      onToggle(index, newActive, step);
    },
    [step, index, onToggle],
  );

  const first = index === 0;
  const last = typeof numberOfSteps !== 'undefined' && index === numberOfSteps - 1;

  return (
    <div className={classnames(styles.stepperItem, { [styles.last]: last })}>
      <StepperItemHeader index={index} label={step.label} active={active} onToggle={handleToggle} />
      <StepperItemContent active={active} first={first} last={last} onNext={handleNext} onBack={handleBack}>
        {renderContent?.(step, index)}
      </StepperItemContent>
    </div>
  );
};

export default React.memo(StepperItem);
