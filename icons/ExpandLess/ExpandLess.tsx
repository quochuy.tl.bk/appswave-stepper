import { SVGIconProps } from '../../types';

const ExpandLess: React.FC<SVGIconProps> = ({
  className = '',
  color = 'currentColor',
  width = 32,
  height = 32,
  ...rest
}) => {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 32 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      {...rest}
    >
      <path
        d="M9.88667 20.5533L16 14.44L22.1133 20.5533L24 18.6666L16 10.6666L8 18.6666L9.88667 20.5533Z"
        fill={color}
      />
    </svg>
  );
};

export default ExpandLess;
