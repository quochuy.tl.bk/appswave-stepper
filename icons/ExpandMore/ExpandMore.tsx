import { SVGIconProps } from '../../types';

const ExpandMore: React.FC<SVGIconProps> = ({
  className = '',
  color = 'currentColor',
  width = 32,
  height = 32,
  ...rest
}) => {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 32 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      {...rest}
    >
      <path
        d="M22.1133 11.4467L16 17.56L9.88667 11.4467L8 13.3334L16 21.3334L24 13.3334L22.1133 11.4467Z"
        fill={color}
      />
    </svg>
  );
};

export default ExpandMore;
