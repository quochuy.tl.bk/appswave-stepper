export interface SVGIconProps extends React.HTMLAttributes<SVGElement> {
  width?: number;
  height?: number;
}

export interface Step {
  label: string;
}
