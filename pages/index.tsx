import { useCallback, useState } from 'react';
import Head from 'next/head';
import { Stepper } from '../components/Stepper';
import { steps } from '../data/steps';
import usePrevious from '../hooks/usePrevious';
import { Step } from '../types';

const getPreviousStepText = (activeStepIndex: number, previousStepIndex?: number) => {
  if (typeof previousStepIndex !== 'undefined' && previousStepIndex > -1) return previousStepIndex;

  return activeStepIndex;
};

export default function Home() {
  const [activeStepIndex, setActiveStepIndex] = useState(0);
  const { ref: previousActiveStepIndex } = usePrevious(activeStepIndex);

  const handleNavigate = useCallback((index: number) => {
    setActiveStepIndex(index);
  }, []);

  const handleFinish = useCallback(() => {
    alert('Finished!');
  }, []);

  const renderContent = useCallback((step: Step, index: number) => {
    return (
      <>
        You have just moved from&nbsp;
        <strong>step {getPreviousStepText(index, previousActiveStepIndex.current) + 1}</strong> to&nbsp;
        <strong>step {index + 1}</strong>.
      </>
    );
  }, [previousActiveStepIndex]);

  return (
    <>
      <Head>
        <title>Stepper</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="main">
        <h2>Obtaining Transporter Qualification Service</h2>
        <Stepper
          steps={steps}
          activeStepIndex={activeStepIndex}
          onNavigate={handleNavigate}
          onFinish={handleFinish}
          renderContent={renderContent}
        />
      </main>
    </>
  );
}
