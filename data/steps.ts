export const steps = [
  {
    label: 'Categorization',
  },
  {
    label: 'Company information',
  },
  {
    label: 'Truck information',
  },
  {
    label: 'Container information',
  },
  {
    label: 'Labor information',
  },
  {
    label: 'Location information',
  },
  {
    label: 'Required documents',
  },
  {
    label: 'Declarations and Commitments',
  },
];
